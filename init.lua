--
-- tangerine.nvim bootstap
--
local tangerine_path = vim.fn.stdpath("data") .. "/lazy/tangerine.nvim"
if not vim.uv.fs_stat(tangerine_path) then
  vim.fn.system({
    "git",
    "clone",
    "https://github.com/udayvir-singh/tangerine.nvim",
    tangerine_path,
  })
end
vim.opt.rtp:prepend(tangerine_path)

local ok, tangerine = pcall(require, "tangerine")
if not ok then
  vim.notify "tangerine.nvim could not be loaded!"
  return
end

tangerine.setup({
  compiler = {
    hooks = {
      "onsave"
    }
  }
})
