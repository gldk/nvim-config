;
; bootstrap lazy.nvim
;
(local lazy-path (.. (vim.fn.stdpath :data) :/lazy/lazy.nvim))

(when (not (vim.uv.fs_stat lazy-path))
  (vim.fn.system [:git
                  :clone
                  "--filter=blob:none"
                  :--single-branch
                  "https://github.com/folke/lazy.nvim.git"
                  lazy-path]))

(vim.opt.rtp:prepend lazy-path)

((. (require :lazy) :setup) :plugins)

(require :core.options)
(require :core.keymappings)
(require :core.autocommands)
(require :core.transparency)
