(fn pack [ident ?options]
  (let [options (or ?options {})]
    (doto options (tset 1 ident))))

{: pack}
