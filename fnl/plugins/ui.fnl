(import-macros {: pack} :util)

(local tabby-theme {:tab :TabLine
                    :win :TabLine
                    :head :TabLine
                    :tail :TabLine
                    :fill :TabLineFill
                    :current_tab :TabLineSel})

(fn tabby-tab [tab]
  {1 (or (and (tab.is_current) " > ") " - ")
   2 (tab.number)
   3 (tab.close_btn " x ")
   :hl (or (and (tab.is_current) tabby-theme.current_tab) tabby-theme.tab)
   :margin " "})

(fn tabby-win [line]
  (fn [win]
    {1 (line.sep " " tabby-theme.win tabby-theme.fill)
     2 (or (and (win.is_current) ">") "-")
     3 (win.buf_name)
     4 (line.sep " " tabby-theme.win tabby-theme.fill)
     :hl tabby-theme.win
     :margin " "}))

(fn tabby-config []
  (let [tabline (require :tabby.tabline)]
    (tabline.set (fn [line]
                   {1 (let [tabs (line.tabs)]
                        (tabs.foreach tabby-tab))
                    2 (line.spacer)
                    3 (let [wins (-> (line.api.get_current_tab)
                                     (line.wins_in_tab))]
                        (wins.foreach (tabby-win line)))
                    :hl tabby-theme.fill}))))

[
 ; theme
 (pack :shaunsingh/nord.nvim
       {:priority 1000 :config #(vim.cmd "colorscheme nord")})

 ; configurable status line
 (pack :nvim-lualine/lualine.nvim
       {:dependencies [:nvim-tree/nvim-web-devicons]
        :opts {:options {:section_separators ""
                         :component_separators ""
                         :theme :nord}
               :sections {:lualine_x [:filetype]
                          :lualine_y []
                          :lualine_z [#(let [col (vim.fn.virtcol ".")
                                             len "%{strwidth(getline('.'))}"]
                                         (string.format "%d/%s" col len))]}}})

 ; indentation guides
 (pack :lukas-reineke/indent-blankline.nvim {:main :ibl :config true})

 ; configurable tabline
 (pack :nanozuki/tabby.nvim {:config tabby-config})

 ; block folds
 (pack :kevinhwang91/nvim-ufo
       {:dependencies [:kevinhwang91/promise-async]
        :opts {:preview {:win_config {:border ["" "-" "" "" "" "-" "" ""]
                                      :winhighlight "Normal:Folded"
                                      :winblend 0}}
               :provider_selector (fn [bn ft bf] [:treesitter :indent])}})

 ; color highlight
 (let [ft [:css :lua :markdown :scss :text :vim :yaml]]
   (pack :nvchad/nvim-colorizer.lua
         {: ft
          :opts {:filetypes ft :user_default_options {:names false}}
          :config #(vim.keymap.set :n "'c" ":ColorizerToggle<CR>"
                                   {:silent true})}))

 (pack :hiphish/rainbow-delimiters.nvim
       {:dependencies [:nvim-treesitter/nvim-treesitter]
        :event [:BufReadPost :BufNewFile]})]
