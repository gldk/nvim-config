(import-macros {: pack} :util)

(fn on_attach [_ buffer]
  (let [nmap (fn [keys func desc]
               (vim.keymap.set :n keys func {: buffer : desc}))]
    (nmap :<leader>rn vim.lsp.buf.rename          "[R]e[n]ame")
    (nmap :<leader>ca vim.lsp.buf.code_action     "[C]ode [A]ction")
    (nmap :gd         vim.lsp.buf.definition      "[G]oto [D]efinition")
    (nmap :gI         vim.lsp.buf.implementation  "[G]oto [I]mplementation")
    (nmap :gD         vim.lsp.buf.declaration     "[G]oto [D]eclaration")
    (nmap :<leader>D  vim.lsp.buf.type_definition "Type [D]efinition")
    (nmap :K          vim.lsp.buf.hover           "Hover Documentation")
    (nmap :<C-k>      vim.lsp.buf.signature_help  "Signature Documentation")

    (nmap :<leader>wa vim.lsp.buf.add_workspace_folder    "[W]orkspace [A]dd Folder")
    (nmap :<leader>wr vim.lsp.buf.remove_workspace_folder "[W]orkspace [R]emove Folder")
    (nmap :<leader>wl #(-> (vim.lsp.buf.list_workspace_folders)
                           (vim.inspect)
                           (print)) "[W]orkspace [L]ist Folders")

    (vim.api.nvim_buf_create_user_command
      buffer :Format #(vim.lsp.buf.format) {:desc "Format current buffer with LSP"})))

(local capabilities
       (let [cmp_nvim_lsp (require :cmp_nvim_lsp)]
         (-> (vim.lsp.protocol.make_client_capabilities)
             (cmp_nvim_lsp.default_capabilities))))

(fn setup-handler [lsp]
  (let [lsp-config (. (require :lspconfig) lsp)]
    (lsp-config.setup {: on_attach : capabilities})))

[
 ; configs for nvim lsp
 (pack :neovim/nvim-lspconfig {:event [:BufReadPre :BufNewFile]})

 ; package manager for language servers, daps, linters and formatters
 (pack :williamboman/mason.nvim {:config true})

 ; mason extension for lsp management
 (pack :williamboman/mason-lspconfig.nvim
       {:config #(let [mason-lsp (require :mason-lspconfig)]
                   (mason-lsp.setup)
                   (mason-lsp.setup_handlers [setup-handler]))})
 ; lsp progress
 (pack :j-hui/fidget.nvim
       {:event :LspAttach :opts {:notification {:window {:winblend 0}}}})

 ; render lsp diagnostics on top (under) line of codes
 (pack "https://git.sr.ht/~whynothugo/lsp_lines.nvim"
       {:config #(vim.diagnostic.config {:virtual_test false
                                         :virtual_lines {:only_current_line true
                                                         :highlight_whole_line true}})})]
