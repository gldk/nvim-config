(import-macros {: pack} :util)

(local kind-icons {:Text ""
                   :Method :m
                   :Function ""
                   :Constructor ""
                   :Field ""
                   :Variable ""
                   :Class ""
                   :Interface ""
                   :Module ""
                   :Property ""
                   :Unit ""
                   :Value ""
                   :Enum ""
                   :Keyword ""
                   :Snippet ""
                   :Color ""
                   :File ""
                   :Reference ""
                   :Folder ""
                   :EnumMember ""
                   :Constant ""
                   :Struct ""
                   :Event ""
                   :Operator ""
                   :TypeParameter ""})

(fn cmp-mapping [cmp]
  (let [replace-termcodes #(vim.api.nvim_replace_termcodes $ true true true)
        pumvisible vim.fn.pumvisible
        feedkeys vim.fn.feedkeys
        mapping cmp.mapping]
    {:<C-Space>
       (mapping (mapping.complete) [:i :c])
     :<C-k>
       (mapping.select_prev_item)
     :<C-j>
       (mapping.select_next_item)
     :<C-b>
       (mapping (mapping.scroll_docs -1) [:i :c])
     :<C-f>
       (mapping (mapping.scroll_docs 1) [:i :c])
     :<C-e>
       (mapping {:i (mapping.abort) :c (mapping.close)})
     :<CR>
       (mapping.confirm {:select true})
     :<Tab>
       (mapping #(if (= (pumvisible) 1)
                        (feedkeys (replace-termcodes :<C-n>) :n)
                        (cmp.visible)
                        (cmp.select_next_item)
                        ($)) [:i :s])
     :<S-Tab>
       (mapping #(if (= (pumvisible) 1)
                        (feedkeys (replace-termcodes :<C-p>) :n)
                        (cmp.visible)
                        (cmp.select_prev_item)
                        ($)) [:i :s])}))

(fn cmp-opts []
  (let [cmp (require :cmp)
        snippy (require :snippy)
        mapping cmp.mapping]
    {:snippet {:expand #(snippy.expand_snippet $.body)}
     :sources [{:name :snippy}
               {:name :buffer}
               {:name :calc}
               {:name :nvim_lsp}
               {:name :nvim_lsp_signature_help}]
     :mapping (cmp-mapping cmp)
     :formatting {:format (fn [entry item]
                            (tset item :kind
                                  (. kind-icons item.kind))
                            (tset item :menu
                                  (. {:nvim_lsp "[L]"
                                      :snippy "[S]"
                                      :buffer "[B]"}
                                     entry.source.name))
                            item)}
     :confirm_opts {:behavior cmp.ConfirmBehavior.Replace :select false}
     :window {:documentation (cmp.config.window.bordered)}}))

[
 ; snippet engine
 (pack :dcampos/nvim-snippy {:event :InsertEnter})

 ; auto-completion plugins
 (pack :hrsh7th/nvim-cmp
       {:dependencies [:hrsh7th/cmp-buffer
                       :hrsh7th/cmp-calc
                       :hrsh7th/cmp-cmdline
                       :hrsh7th/cmp-nvim-lsp
                       :hrsh7th/cmp-nvim-lsp-signature-help
                       :dcampos/cmp-snippy]
        :event :InsertEnter
        :opts cmp-opts
        :config #((. (require :cmp) :setup) $2)})]
