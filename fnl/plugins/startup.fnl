(import-macros {: pack} :util)

[
 (pack :udayvir-singh/tangerine.nvim {:priority 1001 :lazy false})

 (pack :folke/lazy.nvim)

 (pack :nvim-lua/plenary.nvim)

 (pack :nvim-lua/popup.nvim)]
