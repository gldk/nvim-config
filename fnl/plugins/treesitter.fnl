(import-macros {: pack} :util)

(local ensure_installed [; :bash
                         :fish
                         :c
                         :clojure
                         :go
                         :python
                         :rust
                         :zig
                         ; :haskell
                         ; :java
                         ; :lua
                         ; :ruby
                         ; :scala
                         ; :dart
                         ; :perl
                         ; :php
                         ; :vue
                         ; :html
                         ; :css
                         ; :javascript
                         ; :typescript
                         :regex
                         :json
                         :toml
                         :dockerfile])

; nvim-treesitter-textobjects
(local textobjects
       {:select {:enable true
                 :lookahead true
                 :keymaps {:aa "@parameter.outer"
                           :ia "@parameter.inner"
                           :af "@function.outer"
                           :if "@function.inner"
                           :ac "@class.outer"
                           :ic "@class.inner"}}
        :move {:enable true
               :set_jumps true
               :goto_next_start
                 {"]m" "@function.outer"
                  "]]" "@class.outer"}
               :goto_next_end
                 {"]M" "@function.outer"
                  "][" "@class.outer"}
               :goto_previous_start
                 {"[m" "@function.outer"
                  "[[" "@class.outer"}
               :goto_previous_end
                 {"[M" "@function.outer"
                  "[]" "@class.outer"}}
        :swap {:enable true
               :swap_next {:<leader>a "@parameter.inner"}
               :swap_previous {:<leader>A "@parameter.inner"}}})

[
 ; treesitter aka. language parser & syntax highlighter
 (pack :nvim-treesitter/nvim-treesitter
       {:build ":TSUpdate"
        :event [:BufReadPost :BufNewFile]
        :dependencies [:nvim-treesitter/nvim-treesitter-textobjects
                       :windwp/nvim-ts-autotag
                       (pack :RRethy/nvim-treesitter-endwise
                             {:name :nvim-ts-endwise})
                       (pack :nvim-treesitter/playground
                             {:name :nvim-ts-playground})]
        :opts {: ensure_installed
               :sync_install false
               :ignore_install [""]
               :highlight {:enable false
                           :additional_vim_regex_highlighting true}
               :ident {:enable true}
               : textobjects
               ; nvim-ts-autotag
               :autotag {:enable true}
               ; nvim-ts-context-commentstring
               :context_commentstring {:enable true :enable_autocmd false}
               ; nvim-treesitter-endwise
               :endwise {:enable true}}
        :config #((. (require :nvim-treesitter.configs) :setup) $2)})

 ; block spitting/joining
 (pack :Wansmer/treesj
       {:keys [:<space>m :<space>j :<space>s]
        :dependencies [:nvim-treesitter/nvim-treesitter]})]
