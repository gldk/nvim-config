(import-macros {: pack} :util)

[
 ; diagnostics list
 (pack :folke/trouble.nvim {:dependencies [:nvim-tree/nvim-web-devicons]})

 (pack :windwp/nvim-autopairs
       {:event :InsertEnter
        :opts {:disable_filetype [:text :txt] :fast_wrap {}}})
 (pack :kylechui/nvim-surround {:event :VeryLazy})

 ; file explorer
 (pack :nvim-tree/nvim-tree.lua {:dependencies [:nvim-tree/nvim-web-devicons]
                                 :config true})
 ; heuristic tabwidth adjustment
 (pack :tpope/vim-sleuth)

 ; smart comment plugin
 (pack :numToStr/Comment.nvim)]
