(local autocmd vim.api.nvim_create_autocmd)

; Remove trailing whitespaces
(autocmd [:BufWritePre] {:pattern "" :command "%s/\\s\\+$//e"})

; fix unrestored terminal cursor style
(autocmd [:VimLeave :VimSuspend] {:command "set guicursor=n-v-c-sm:ver20"})

(autocmd [:VimEnter :VimResume] {:command "set guicursor"})
