(vim.api.nvim_set_keymap "" :<Space> :<Nop> {:noremap true :silent true})

(set vim.g.mapleader " ")
(set vim.g.maplocalleader " ")
