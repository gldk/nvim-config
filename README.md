Neovim config written in Fennel

## Index
1. [Structure](#structure)
1. [Plugins](#plugins)
1. [Setup](#setup)

## Structure
```
├── init.lua       | tangerine.nvim initialization file.
├── init.fnl       | lazy.nvim initialization file.
└── fnl/           |
     ├── core/     | options, mapping, etc.
     ├── plugins/  | plugins
     └── util.fnl  | macros
```
## Plugins
- startup
  - [tangerine](https://github.com/udayvir-singh/tangerine.nvim) fennel integration to nvim
  - [lazy](https://github.com/folke/lazy.nvim) plugin management
  - [plenary](https://github.com/nvim-lua/plenary.nvim) functions used by other plugins
  - [popup](https://github.com/nvim-lua/popup.nvim) vim popup api implementation used by other plugins

- ui
  - [nord](https://github.com/shaunsingh/nord.nvim) colorscheme
  - [lualine](https://github.com/nvim-lualine/lualine.nvim) configurable statusline
  - [tabby](https://github.com/nanozuki/tabby.nvim) configurable tabline
  - [rainbow-delimiters](https://github.com/HiPhish/rainbow-delimiters.nvim)
  - [nvim-ufo](https://github.com/kevinhwang91/nvim-ufo) block folds
  - [indent-blankline](https://github.com/lukas-reineke/indent-blankline.nvim) indentation guides
  - [colorizer](https://github.com/nvchad/nvim-colorizer.lua) color highlighter

- qol
  - [trouble](https://github.com/folke/trouble.nvim) diagnostics list
  - [autopairs](https://github.com/windwp/nvim-autopairs)
  - [surround](https://github.com/kylechui/nvim-surround)
  - [nvim-tree](https://github.com/nvim-tree/nvim-tree.lua) file explorer
  - [vim sleuth](https://github.com/tpope/vim-sleuth) heuristic tabwidth adjustment
  - [comment](https://github.com/numToStr/Comment.nvim) smart comment plugin

- cmp
  - [cmp](https://github.com/hrsh7th/nvim-cmp) auto-completion
    - [buffer](https://github.com/hrsh7th/cmp-buffer)
    - [calc](https://github.com/hrsh7th/cmp-calc)
    - [cmdline](https://github.com/hrsh7th/cmp-cmdline)
    - [lsp](https://github.com/hrsh7th/cmp-nvim-lsp)
    - [lsp-signature-help](https://github.com/hrsh7th/cmp-nvim-lsp-signature-help)
  - [snippy](https://github.com/dcampos/nvim-snippy) snippet plugin
    - [cmp-snippy](https://github.com/dcampos/cmp-snippy)

- lsp
  - [lspconfig](https://github.com/neovim/nvim-lspconfig) configs for neovim lsp client
  - [mason](https://github.com/williamboman/mason.nvim) package manager for lsp/dap servers, linters and formatters
    - [mason-lspconfig](https://github.com/williamboman/mason-lspconfig.nvim) mason extension for easier mason + lspconfig setup
  - [fidget](https://github.com/j-hui/fidget.nvim) lsp progress
  - [lsp-lines](https://git.sr.ht/~whynothugo/lsp_lines.nvim) lsp diagnostics on virtual lines on top of code

- treesitter
  - [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter) syntax highlighting
    - [textobjects](https://github.com/nvim-treesitter/nvim-treesitter-textobjects)
    - [autotag](https://github.com/windwp/nvim-ts-autotag)
    - [treesj](https://github.com/Wansmer/treesj) block splitting/folding
    - [endwise](https://github.com/RRethy/nvim-treesitter-endwise)
    - [playground](https://github.com/nvim-treesitter/playground)

- dependencies
  - [nvim-web-devicons](https://github.com/nvim-tree/nvim-web-devicons) nerd icons used by other plugins




## Setup
Tangerine should compile all .fnl config files to lua on the first run.
Lazy should install plugins on its first run.

Treesitter should install language parsers set under `ensure_installed` table in its config file.
Additional parsers can be viewed with `:TSInstallInfo` and installed with `:TSInstall <parser>`.

Language Servers, DAP, formatters and linters can be installed in `:Mason` window.
